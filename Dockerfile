FROM ubuntu
RUN apt-get update -y

WORKDIR /app

COPY . /app

RUN apt-get update && apt-get install --no-install-recommends -y \
    python3 python3-pip wget && \
    pip3 install -r requirements.txt

ENTRYPOINT [ "python3" ]

CMD ["app.py"]